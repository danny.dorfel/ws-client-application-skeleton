import Connection from './websocket/Connection';
import RedisMessageHandler from './websocket/RedisMessageHandler';

const connection = new Connection('ws://127.0.0.1:3000/ws');
connection.setMessageHandler(new RedisMessageHandler());
connection.connect();
