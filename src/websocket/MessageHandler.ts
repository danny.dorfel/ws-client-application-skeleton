export default interface MessageHandler {
    handle(message: string): void;
}
