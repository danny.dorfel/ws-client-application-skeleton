import MessageHandler from './MessageHandler';

export default class Connection {
    private static RECONNECT_DELAY = 1000;

    readonly #url: string;

    #readyState = -1;

    #socket: WebSocket | undefined;

    #messageHandler: MessageHandler | undefined;

    constructor(url: string) {
        this.#url = url;
    }

    setMessageHandler(handler: MessageHandler): void {
        this.#messageHandler = handler;
    }

    connect(): void {
        if (this.#socket) return;

        this.#readyState = WebSocket.CONNECTING;
        this.#socket = new WebSocket(this.#url);
        this.#addEventListeners();
    }

    terminate(): void {
        if (this.#readyState === WebSocket.CLOSED) return;

        if (this.#socket) {
            this.#readyState = WebSocket.CLOSING;
            this.#socket.close();
        }
    }

    send(message: string): boolean {
        if (!this.#socket || this.#readyState !== WebSocket.OPEN) return false;

        this.#socket.send(message);
        return true;
    }

    #openHandler = (): void => {
        this.#readyState = WebSocket.OPEN;
    };

    #closeHandler = (): void => {
        this.#readyState = WebSocket.CLOSED;
        this.#removeEventListeners();
        this.#socket = undefined;

        window.setTimeout(() => this.connect(), Connection.RECONNECT_DELAY);
    };

    #handleMessage = (message: MessageEvent): void => {
        this.#messageHandler?.handle(message.data.toString());
    };

    #addEventListeners(): void {
        if (!this.#socket) return;

        this.#socket.addEventListener('open', this.#openHandler);
        this.#socket.addEventListener('close', this.#closeHandler);
        this.#socket.addEventListener('message', this.#handleMessage);
    }

    #removeEventListeners(): void {
        if (!this.#socket) return;

        this.#socket.removeEventListener('open', this.#openHandler);
        this.#socket.removeEventListener('close', this.#closeHandler);
        this.#socket.removeEventListener('message', this.#handleMessage);
    }
}
