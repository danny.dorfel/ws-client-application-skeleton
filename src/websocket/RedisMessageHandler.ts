import MessageHandler from './MessageHandler.js';

export default class RedisMessageHandler implements MessageHandler {
    handle(message: string): void {
        console.log(message);
    }
}
