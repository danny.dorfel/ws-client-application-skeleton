import './styles/app.pcss';
import './app';

if (module.hot) {
    module.hot.accept();
}
